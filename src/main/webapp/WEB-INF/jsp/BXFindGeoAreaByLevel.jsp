<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="en">
<head>
    <title>Find Geo Area By Level</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
<section id="search" class="section">
    <header class="imageheader"></header>
    <div class="container">
        <h2 class="headline">Search by level</h2>
        <form action="/BXFindGeoAreaByLevel" method="get">
            <label class="card-title">Search what geographic area level</label>
            <input path="BXFindGeoAreaByLevel" name="level" value="">
            <input type="submit" value="Search">
        </form>
    </div>
</section>
<table>
    <tr>
        <th>geographic area id</th>
        <th>code</th>
        <th>level</th>
        <th>name</th>
        <th>alternative code</th>
    </tr>
        <c:if test="${!empty(geographicAreas)}">
        <section id="geographicAreas" class="section">
        <c:forEach var="geographicArea" items="${geographicAreas}">
        <div class="geographicAreaContainer">

            <div class="geographicAreaContainerItem">
              <tr>
               <td><input type = "text" name="code" value="${geographicArea.geographicAreaId}"></td>
              <td> <input type ="text" name ="code" value="${geographicArea.code}"></td>
               <td> <input type="text" name="level" value="${geographicArea.level}"></td>
               <td> <input type="text" name="name" value="${geographicArea.name}"> </td>
               <td> <input type="text" name="alternativeCode" value="${geographicArea.alternativeCode}"> </td>
              </tr>
            </div>

            </c:forEach>
        </div>
        </table>
    </section>
</c:if>
</body>
</html>
