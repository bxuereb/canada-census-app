<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="en">
<head>
    <title>GEOGRAPHIC AREA DETAILS</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
<section id="search" class="section">
    <header class="imageheader"></header>
    <div class="container">
        <h2 class="headline">Search by name</h2>
        <form action="/BXGeoAreaDetails" method="get">
            <label class="card-title">Search what geographic area name</label>
            <input path="BXGeoAreaDetails" name="name" value="">
            <input type="submit" value="Search">
        </form>
    </div>
</section>
<table>
    <tr>
        <th>Name</th>
        <th>Code</th>
        <th>Alternative Code</th>
        <th>Level</th>
        <th>Total Population</th>
    </tr>
    <c:if test="${!empty(geographicAreas)}">
    <section id="geographicAreas" class="section">
        <c:forEach var="geographicArea" items="${geographicAreas}">

        <div class="geographicAreaContainer">

            <div class="geographicAreaContainerItem">
                <tr>
                    <td><input type = "text" name="name" value="${geographicArea.name}"></td>
                    <td> <input type ="text" name ="code" value="${geographicArea.code}"></td>
                    <td> <input type="text" name="alternativeCode" value="${geographicArea.alternativeCode}"></td>
                    <td> <input type="text" name="level" value="${geographicArea.level}"> </td>

                </tr>
            </div>

            </c:forEach>
        </div>
</table>
</section>
</c:if>
</body>