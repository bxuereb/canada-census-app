package com.example.BXCensusApp.DAL;

import com.example.BXCensusApp.beans.Geographicarea;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class BXGeoAreaDB
{
    public static List<Geographicarea> selectGeographicAreaByLevel(String level) {

        System.out.println("In CensusDB: selectGeographicAreaByLevel() ");

        // load the db driver (not singleton pattern: for demo only)
        try {
            // Class.forName("com.mysql.jdbc.Driver");
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            System.err.println(e);
            return null;
        }

        // set the db connection parameter values (hard-coded for demo only)
        String dbURL = "jdbc:mysql://localhost:3306/censusdb";
        String username = "root";
        String password = "root";

        // construct the query
        String query = "SELECT * FROM geographicarea WHERE level = " + level + " ";
        System.out.println(query);

        Connection connection = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            connection = DriverManager.getConnection(
                    dbURL, username, password);

            ps = connection.prepareStatement(query);
            rs = ps.executeQuery();
            ArrayList<Geographicarea> geographicAreas = new ArrayList<>();
            while (rs.next()) {
                Geographicarea g = new Geographicarea();
                g.setLevel(rs.getInt("level"));
                g.setName(rs.getString("name"));
                g.setCode(rs.getInt("code"));
                g.setAlternativeCode(rs.getInt("alternativeCode"));
                geographicAreas.add(g);
            }
            rs.close();
            connection.close();
            return geographicAreas;
        } catch (SQLException e) {
            System.err.println(e);
            return null;
        }
    }


    public static List<Geographicarea> selectGeographicAreaByKeyword(String keyword) {

        System.out.println("In CensusDB: selectGeographicAreaByKeyword() ");

        // load the db driver (not singleton pattern: for demo only)
        try {
            // Class.forName("com.mysql.jdbc.Driver");
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            System.err.println(e);
            return null;
        }

        // set the db connection parameter values (hard-coded for demo only)
        String dbURL = "jdbc:mysql://localhost:3306/censusdb";
        String username = "root";
        String password = "root";

        // construct the query
        String query = "SELECT * FROM geographicarea WHERE name LIKE '%" + keyword + "%' ";
        System.out.println(query);

        Connection connection = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            connection = DriverManager.getConnection(
                    dbURL, username, password);

            ps = connection.prepareStatement(query);
            rs = ps.executeQuery();
            ArrayList<Geographicarea> geographicAreas = new ArrayList<>();
            while (rs.next()) {
                Geographicarea g = new Geographicarea();
                g.setLevel(rs.getInt("level"));
                g.setName(rs.getString("name"));
                g.setCode(rs.getInt("code"));
                g.setAlternativeCode(rs.getInt("alternativeCode"));
                geographicAreas.add(g);
            }
            rs.close();
            connection.close();
            return geographicAreas;
        } catch (SQLException e) {
            System.err.println(e);
            return null;
        }
    }

    public static List<Geographicarea> selectGeographicAreaByName(String name) {

        System.out.println("In CensusDB: selectGeographicAreaByName() ");

        // load the db driver (not singleton pattern: for demo only)
        try {
            // Class.forName("com.mysql.jdbc.Driver");
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            System.err.println(e);
            return null;
        }

        // set the db connection parameter values (hard-coded for demo only)
        String dbURL = "jdbc:mysql://localhost:3306/censusdb";
        String username = "root";
        String password = "root";

        // construct the query
        String query = "SELECT * FROM geographicarea WHERE name ='" + name + "' ";

        System.out.println(query);


        Connection connection = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            connection = DriverManager.getConnection(
                    dbURL, username, password);

            ps = connection.prepareStatement(query);
            rs = ps.executeQuery();
            ArrayList<Geographicarea> geographicAreas = new ArrayList<>();
            while (rs.next()) {
                Geographicarea g = new Geographicarea();
                g.setLevel(rs.getInt("level"));
                g.setName(rs.getString("name"));
                g.setCode(rs.getInt("code"));
                g.setAlternativeCode(rs.getInt("alternativeCode"));
                geographicAreas.add(g);
            }


            rs.close();
            connection.close();
            return geographicAreas;
        } catch (SQLException e) {
            System.err.println(e);
            return null;
        }
    } {
}
}

