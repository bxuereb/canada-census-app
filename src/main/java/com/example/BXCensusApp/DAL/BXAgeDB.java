package com.example.BXCensusApp.DAL;

import com.example.BXCensusApp.beans.Age;

import java.sql.*;

public class BXAgeDB {


    public static Age selectCombinedAge() {
        System.out.println("In CensusDB: selectAge() ");

        // load the db driver (not singleton pattern: for demo only)
        try {
            // Class.forName("com.mysql.jdbc.Driver");
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            System.err.println(e);
            return null;
        }

        // set the db connection parameter values (hard-coded for demo only)
        String dbURL = "jdbc:mysql://localhost:3306/censusdb";
        String username = "root";
        String password = "root";

        // construct the query

        String query2 = "SELECT * FROM age WHERE censusYear = 1 AND ageGroup=1";

        System.out.println(query2);

        Connection connection = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        ResultSet rs2 = null;
        try {
            connection = DriverManager.getConnection(
                    dbURL, username, password);


            Age a = new Age();

            ps = connection.prepareStatement(query2);
            rs2 = ps.executeQuery();
            rs2.next();
            a.setCombined(rs2.getInt("combined"));

            rs.close();
            connection.close();
            return a;
        } catch (SQLException e) {
            System.err.println(e);
            return null;
        }
    }
}
