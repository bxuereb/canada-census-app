package com.example.BXCensusApp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BXCensusAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(BXCensusAppApplication.class, args);
	}

}
