package com.example.BXCensusApp.beans;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Objects;

@Entity
public class Agegroup {
    private int ageGroupId;
    private String description;

    @Id
    @Column(name = "ageGroupID", nullable = false)
    public int getAgeGroupId() {
        return ageGroupId;
    }

    public void setAgeGroupId(int ageGroupId) {
        this.ageGroupId = ageGroupId;
    }

    @Basic
    @Column(name = "description", nullable = false, length = 40)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Agegroup agegroup = (Agegroup) o;
        return ageGroupId == agegroup.ageGroupId &&
                Objects.equals(description, agegroup.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ageGroupId, description);
    }
}
