package com.example.BXCensusApp.beans;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Objects;

@Entity
public class Age {
    private int ageId;
    private int combined;
    private int male;
    private int female;

    @Id
    @Column(name = "ageID", nullable = false)
    public int getAgeId() {
        return ageId;
    }

    public void setAgeId(int ageId) {
        this.ageId = ageId;
    }

    @Basic
    @Column(name = "combined", nullable = false)
    public int getCombined() {
        return combined;
    }

    public void setCombined(int combined) {
        this.combined = combined;
    }

    @Basic
    @Column(name = "male", nullable = false)
    public int getMale() {
        return male;
    }

    public void setMale(int male) {
        this.male = male;
    }

    @Basic
    @Column(name = "female", nullable = false)
    public int getFemale() {
        return female;
    }

    public void setFemale(int female) {
        this.female = female;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Age age = (Age) o;
        return ageId == age.ageId &&
                combined == age.combined &&
                male == age.male &&
                female == age.female;
    }

    @Override
    public int hashCode() {
        return Objects.hash(ageId, combined, male, female);
    }
}
