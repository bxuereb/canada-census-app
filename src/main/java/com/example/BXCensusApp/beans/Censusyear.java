package com.example.BXCensusApp.beans;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Objects;

@Entity
public class Censusyear {
    private int censusYearId;
    private int censusYear;

    @Id
    @Column(name = "censusYearID", nullable = false)
    public int getCensusYearId() {
        return censusYearId;
    }

    public void setCensusYearId(int censusYearId) {
        this.censusYearId = censusYearId;
    }

    @Basic
    @Column(name = "censusYear", nullable = false)
    public int getCensusYear() {
        return censusYear;
    }

    public void setCensusYear(int censusYear) {
        this.censusYear = censusYear;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Censusyear that = (Censusyear) o;
        return censusYearId == that.censusYearId &&
                censusYear == that.censusYear;
    }

    @Override
    public int hashCode() {
        return Objects.hash(censusYearId, censusYear);
    }
}
