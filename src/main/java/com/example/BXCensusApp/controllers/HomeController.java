package com.example.BXCensusApp.controllers;

import com.example.BXCensusApp.beans.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;

import java.util.Arrays;
import java.util.List;

@Controller
public class HomeController {

    @GetMapping("/home")
    public String goHome(){
        System.out.println("in home controller");
        return "Index";
    }

    @GetMapping("/goToGeoAreaLevel")
    public String goToGeoAreaLevel(){
        System.out.println("going to geo area by level");
        return "BXFindGeoAreaByLevel";
    }

    @GetMapping("/goToGeoAreaKeyword")
    public String goToGeoAreaKeyword(){
        System.out.println("going to login page");
        return "BXFindGeoAreaByKeyword";
    }

    @GetMapping("/goToGeoDetails")
    public String goToGeoDetails(){
        System.out.println("going to details");
        return "BXGeoAreaDetails";
    }


 }
