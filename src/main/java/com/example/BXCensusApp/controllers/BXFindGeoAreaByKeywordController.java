package com.example.BXCensusApp.controllers;

import com.example.BXCensusApp.DAL.BXGeoAreaDB;
import com.example.BXCensusApp.DAL.CensusDB;
import com.example.BXCensusApp.beans.Geographicarea;
import org.springframework.ui.Model;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;
@Controller
public class BXFindGeoAreaByKeywordController {



    @GetMapping("/BXFindGeoAreaByKeyword")
    public String keyword(@RequestParam("keyword") String keyword, Model model){
        System.out.println("in find geo area by keyword controller");
        System.out.println("search criteria: "+keyword);

        List<Geographicarea> geographicAreas = new ArrayList<>();
        //  products = productRepository.searchByName(search);
        geographicAreas = BXGeoAreaDB.selectGeographicAreaByKeyword(keyword);
        model.addAttribute("geographicAreas", geographicAreas);
        return "BXFindGeoAreaByKeyword";
    }

}

