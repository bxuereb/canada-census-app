package com.example.BXCensusApp.controllers;

import com.example.BXCensusApp.DAL.BXGeoAreaDB;
import com.example.BXCensusApp.DAL.CensusDB;
import com.example.BXCensusApp.beans.Geographicarea;
import org.springframework.ui.Model;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;
@Controller
public class BXFindGeoAreaByLevelController {

    @GetMapping("/BXFindGeoAreaByLevel")
public String level(@RequestParam("level") String level, Model model){
    System.out.println("in find geo area by level controller");
    System.out.println("search criteria: "+level);

    List<Geographicarea> geographicAreas = new ArrayList<>();
    //  products = productRepository.searchByName(search);
    geographicAreas = BXGeoAreaDB.selectGeographicAreaByLevel(level);
    model.addAttribute("geographicAreas", geographicAreas);
    return "BXFindGeoAreaByLevel";
}

}