package com.example.BXCensusApp.controllers;

import com.example.BXCensusApp.DAL.BXAgeDB;
import com.example.BXCensusApp.DAL.BXGeoAreaDB;
import com.example.BXCensusApp.DAL.CensusDB;
import com.example.BXCensusApp.beans.Age;
import com.example.BXCensusApp.beans.Geographicarea;
import org.springframework.ui.Model;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;
@Controller
public class BXGeoAreaDetailsController {
    @GetMapping("/BXGeoAreaDetails")
    public String name(@RequestParam("name") String name, Model model){
        System.out.println("in find geo area by name controller");
        System.out.println("search criteria: "+name);

        List<Geographicarea> geographicAreas = new ArrayList<>();
       // Age age;
        //  products = productRepository.searchByName(search);
        geographicAreas = BXGeoAreaDB.selectGeographicAreaByName(name);
       // age = BXAgeDB.selectCombinedAge();
       // model.addAttribute("age",age);
        model.addAttribute("geographicAreas", geographicAreas);
        return "BXGeoAreaDetails";
    }

}